﻿using AutoMapper;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Data.Mappers
{
    public class DTOMapper
    {
        static DTOMapper()
        {
            Mapper.CreateMap<Customer, CustomerDTO>();
            Mapper.CreateMap<CustomerDTO, Customer>();
            Mapper.CreateMap<Order, OrderDTO>().AfterMap((o, dto) => dto.CustomerName = o.Customer.ToString());
            Mapper.CreateMap<OrderDTO, Order>();
        }

        public static TDestination Map<TSource, TDestination>(object source)
        {
            return (TDestination)Mapper.Map(source, typeof(TSource), typeof(TDestination));
        }

        public static TDestination Map<TSource, TDestination>(object source, object destination)
        {
            return (TDestination)Mapper.Map(source, destination, typeof(TSource), typeof(TDestination));
        }
    }
}
