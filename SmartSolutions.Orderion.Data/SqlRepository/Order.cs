﻿using System;
using System.Linq;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Data.Mappers;

namespace SmartSolutions.Orderion.Data
{
    public partial class SqlRepository
    {
        public IQueryable<Order> Orders
        {
            get { return Db.Orders; }
        }

        public OrderDTO CreateOrder(OrderDTO orderDto, string customerId)
        {
            if (orderDto.OrderID == 0)
            {
                orderDto.OrderDate = DateTime.Today;
                var order = DTOMapper.Map<OrderDTO, Order>(orderDto);
                var customer = Db.Customers.FirstOrDefault(c => c.CustomerID == customerId);
                if (customer == null)
                    return null;
                customer.Orders.Add(order);
                Db.Orders.InsertOnSubmit(order);
                Db.Orders.Context.SubmitChanges();

                return DTOMapper.Map<Order, OrderDTO>(Db.Orders.FirstOrDefault(o => o.OrderID == order.OrderID));
            }
            return null;
        }

        public OrderDTO UpdateOrder(OrderDTO order)
        {
            var cache = Db.Orders.FirstOrDefault(x => x.OrderID == order.OrderID);
            if (cache != null)
            {
                cache.CopyFrom(order);
                Db.Orders.Context.SubmitChanges();
                return DTOMapper.Map<Order, OrderDTO>(cache);
            }
            return null;
        }

        public bool DeleteOrder(int orderId)
        {
            var order = Db.Orders.FirstOrDefault(x => x.OrderID == orderId);
            if (order != null)
            {
                Db.Orders.DeleteOnSubmit(order);
                Db.Orders.Context.SubmitChanges();
                return true;
            }

            return false;
        }
    }
}
