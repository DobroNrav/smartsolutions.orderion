﻿using System;
using System.Linq;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Data.Mappers;

namespace SmartSolutions.Orderion.Data
{
    public partial class SqlRepository
    {
        public IQueryable<Customer> Customers
        {
            get { return Db.Customers; }
        }

        public CustomerDTO CreateCustomer(CustomerDTO customerDto)
        {
            if (customerDto.CustomerID == null)
            {
                var customer = DTOMapper.Map<CustomerDTO, Customer>(customerDto);
                customer.CustomerID = GetCustomerStringId();
                Db.Customers.InsertOnSubmit(customer);
                Db.Customers.Context.SubmitChanges();
                return DTOMapper.Map<Customer, CustomerDTO>(Db.Customers.FirstOrDefault(c => c.CustomerID == customer.CustomerID));
            }
            return null;
        }

        public CustomerDTO UpdateCustomer(CustomerDTO customer)
        {
            var cache = Db.Customers.FirstOrDefault(x => x.CustomerID == customer.CustomerID);
            if (cache != null)
            {
                cache.CopyFrom(customer);
                Db.Customers.Context.SubmitChanges();
                return DTOMapper.Map<Customer, CustomerDTO>(cache);
            }
            return null;
        }

        public bool DeleteCustomer(string customerId)
        {
            var customer = Db.Customers.FirstOrDefault(c => c.CustomerID == customerId);
            if (customer != null)
            {
                Db.Orders.DeleteAllOnSubmit(customer.Orders);
                Db.Customers.DeleteOnSubmit(customer);
                Db.Customers.Context.SubmitChanges();
                return true;
            }

            return false;
        }

        private string GetCustomerStringId()
        {
            var ids = Db.Customers.Select(c => c.CustomerID).OrderBy(x => x).ToList();
            for(var j = 0; j < ids.Count; j++)
            {
                var nextId = false;
                var id = ids[j];
                var res = id;
                for (int i = id.Length - 1; i >= 0 ; i--)
                {
                    var c = id[i];
                    while (c < 'Z')
                    {
                        c = (char) (c + 1);
                        res = res.Remove(i, 1).Insert(i, new string(new[]{c}));
                        if (String.Compare(ids[j + 1], res, StringComparison.Ordinal) > 0)
                            return res;
                        else
                        {
                            nextId = true;
                            break;
                        }
                    }
                    if (nextId)
                        break;
                    res = res.Remove(i, 1).Insert(i, "A");
                }
            }
            return "FFFFF";
        }


    }
}
