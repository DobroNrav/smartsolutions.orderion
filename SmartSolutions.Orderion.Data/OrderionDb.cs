namespace SmartSolutions.Orderion.Data
{
    partial class OrderionDbDataContext
    {
        
    }

    public partial class Customer
    {
        public override string ToString()
        {
            return string.Format("{0} ({1}, {2})", CompanyName, City, Country);
        }
    }
}
