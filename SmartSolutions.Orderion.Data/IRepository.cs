﻿using System;
using System.Linq;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Data
{
    public interface IRepository : IDisposable
    {
        #region Customer

        IQueryable<Customer> Customers { get; }

        CustomerDTO CreateCustomer(CustomerDTO customer);

        CustomerDTO UpdateCustomer(CustomerDTO customer);

        bool DeleteCustomer(string customerId);

        #endregion

        #region Order

        IQueryable<Order> Orders { get; }

        OrderDTO CreateOrder(OrderDTO dto, string customerId);

        OrderDTO UpdateOrder(OrderDTO order);

        bool DeleteOrder(int orderId);

        #endregion
    }
}