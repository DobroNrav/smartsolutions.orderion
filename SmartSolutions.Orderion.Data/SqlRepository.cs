﻿
using System.Configuration;
using SmartSolutions.Orderion.Data.Mappers;

namespace SmartSolutions.Orderion.Data
{
    public partial class SqlRepository : IRepository
    {
        public static OrderionDbDataContext Db { get; set; }

        public SqlRepository()
        {
            Db =
                new OrderionDbDataContext(
                    ConfigurationManager.ConnectionStrings["NorthwindConnectionString"].ConnectionString);
        }

        public void Dispose()
        {
            Db.Dispose();
            Db =
                new OrderionDbDataContext(
                    ConfigurationManager.ConnectionStrings["NorthwindConnectionString"].ConnectionString);
        }
    }
}
