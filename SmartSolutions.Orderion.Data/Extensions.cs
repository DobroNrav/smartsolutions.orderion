﻿using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Data.Mappers;

namespace SmartSolutions.Orderion.Data
{
    public static class Extensions
    {
        public static void CopyFrom(this Customer customer, CustomerDTO dto)
        {
            DTOMapper.Map<CustomerDTO, Customer>(dto, customer);
            //customer.Address = dto.Address;
            //customer.City = dto.City;
            //customer.CompanyName = dto.CompanyName;
            //customer.ContactName = dto.ContactName;
            //customer.ContactTitle = dto.ContactTitle;
            //customer.Country = dto.Country;
            //customer.Fax = dto.Fax;
            //customer.Phone = dto.Phone;
            //customer.PostalCode = dto.PostalCode;
            //customer.Region = dto.Region;
        }

        public static void CopyFrom(this Order order, OrderDTO dto)
        {
            DTOMapper.Map<OrderDTO, Order>(dto, order);
            //order.Freight = dto.Freight;
            //order.RequiredDate = dto.RequiredDate;
            //order.ShipAddress = dto.ShipAddress;
            //order.ShipCity = dto.ShipCity;
            //order.ShipCountry = dto.ShipCountry;
            //order.ShipName = dto.ShipName;
            //order.ShipPostalCode = dto.ShipPostalCode;
            //order.ShipRegion = dto.ShipRegion;
            //order.ShippedDate = dto.ShippedDate;
        }
    }
}
