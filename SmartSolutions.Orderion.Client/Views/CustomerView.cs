﻿using System;
using System.Windows.Forms;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces.Customer;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.Views
{
    public partial class CustomerView : Form, ICustomerView
    {
        private CustomerDTO _customer;

        public CustomerView()
        {
            InitializeComponent();
        }

        public void Init(CustomerDTO customer)
        {
            _customer = customer;
            LoadAttributes(customer);
        }

        public event Action<CustomerDTO> Save;

        private void LoadAttributes(CustomerDTO customer)
        {
            tbCompanyName.Text = customer.CompanyName;
            tbContactName.Text = customer.ContactName;
            tbContactTitle.Text = customer.ContactTitle;
            tbPhone.Text = customer.Phone;
            tbFax.Text = customer.Fax;
            tbCountry.Text = customer.Country;
            tbRegion.Text = customer.Region;
            tbCity.Text = customer.City;
            tbAddress.Text = customer.Address;
            tbPostalCode.Text = customer.PostalCode;
        }

        private void SaveAttributes()
        {
            _customer.CompanyName = tbCompanyName.Text;
            _customer.ContactName = tbContactName.Text;
            _customer.ContactTitle = tbContactTitle.Text;
            _customer.Phone = tbPhone.Text;
            _customer.Fax = tbFax.Text;
            _customer.Country = tbCountry.Text;
            _customer.Region = tbRegion.Text;
            _customer.City = tbCity.Text;
            _customer.Address = tbAddress.Text;
            _customer.PostalCode = tbPostalCode.Text;
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            SaveAttributes();
            if (Save != null)
                Save(_customer);
            DialogResult = DialogResult.OK;
        }
    }
}
