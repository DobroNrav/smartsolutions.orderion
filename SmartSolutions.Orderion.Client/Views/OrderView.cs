﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartSolutions.Orderion.Client.ViewInterfaces.Order;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.Views
{
    public partial class OrderView : Form, IOrderView
    {
        private OrderDTO _order;

        public OrderView()
        {
            InitializeComponent();
        }

        public event Action<OrderDTO> Save;

        public void Init(OrderDTO order)
        {
            _order = order;
            LoadAttributes(order);
        }

        private void LoadAttributes(OrderDTO order)
        {
            if (order.OrderID != 0)
            {
                dateTimeRequired.Value = order.RequiredDate ?? DateTime.Now;
                if (order.ShippedDate.HasValue)
                {
                    dateTimeShipped.Value = order.ShippedDate.Value;
                    dateTimeShipped.Checked = true;
                }
                if (order.Freight.HasValue)
                    nudFreight.Value = order.Freight.Value;
                tbCountry.Text = order.ShipCountry;
                tbRegion.Text = order.ShipRegion;
                tbCity.Text = order.ShipCity;
                tbAddress.Text = order.ShipAddress;
                tbPostalCode.Text = order.ShipPostalCode;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SaveAttributes();
            if (Save != null)
                Save(_order);
            DialogResult = DialogResult.OK;
        }

        private void SaveAttributes()
        {
            _order.RequiredDate = dateTimeRequired.Checked ? dateTimeRequired.Value : (DateTime?) null;
            _order.ShippedDate = dateTimeShipped.Checked ? dateTimeShipped.Value : (DateTime?) null;
            _order.Freight = nudFreight.Value > 0 ? nudFreight.Value : (decimal?) null;
            _order.ShipCountry = tbCountry.Text;
            _order.ShipRegion = tbRegion.Text;
            _order.ShipCity = tbCity.Text;
            _order.ShipAddress = tbAddress.Text;
            _order.ShipPostalCode = tbPostalCode.Text;
        }
    }
}
