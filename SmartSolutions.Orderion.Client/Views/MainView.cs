﻿using System;
using System.Windows.Forms;
using SmartSolutions.Orderion.Client.ViewInterfaces;

namespace SmartSolutions.Orderion.Client.Views
{
    public partial class MainView : Form, IMainView
    {
        public MainView()
        {
            InitializeComponent();
        }

        #region Public Methods

        public void AddPage(IPageVeiw pageView)
        {
            var tabPage = new TabPage(pageView.GetTitle());
            var control = pageView as Control;
            if (control == null)
                return;
            control.Dock = DockStyle.Fill;
            tabPage.Controls.Add(control);
            mainTabControl.TabPages.Add(tabPage);
            mainTabControl.SelectTab(tabPage);
        }

        public object GetCurrentObject()
        {
            if (mainTabControl.SelectedTab == null || mainTabControl.SelectedTab.Controls.Count == 0) return null;
            var page = mainTabControl.SelectedTab.Controls[0] as IPageVeiw;
            if (page == null) return null;
            return page.GetCurrentObject();
        }

        #endregion

        #region Events

        public event Action ShowAllCustomers;
        public event Action AddCustomer;
        public event Action EditCustomer;
        public event Action DeleteCustomer;

        public event Action ShowAllOrders;
        public event Action AddOrder;
        public event Action EditOrder;
        public event Action DeleteOrder;
        
        #endregion

        #region Event Handlers

        private void allCustomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ShowAllCustomers != null)
                ShowAllCustomers();
        }

        private void addCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (AddCustomer != null)
                AddCustomer();
        }

        private void editCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (EditCustomer != null)
                EditCustomer();
        }

        private void deleteCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DeleteCustomer != null)
                DeleteCustomer();
        }

        private void allOrdersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ShowAllOrders != null)
                ShowAllOrders();
        }

        private void addOrderToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (AddOrder != null)
                AddOrder();
        }

        private void editOrderToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (EditOrder != null)
                EditOrder();
        }

        private void deleteOrderToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (DeleteOrder != null)
                DeleteOrder();
        }

        #endregion

    }
}
