﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces.Customer;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.Views.Controls
{
    public partial class CustomersControl : UserControl, ICustomersView
    {
        #region Constructors

        public CustomersControl()
        {
            InitializeComponent();
            dataGridCustomers.AutoGenerateColumns = false;
        }

        #endregion

        #region Events

        public event Action<CustomerDTO> EditCustomer;
        public event Action AddCustomer;
        public event Action<CustomerDTO> DeleteCustomer;
        public event Action<CustomerDTO> AddOrder;

        #endregion

        #region Public Methods

        public void FillData(IEnumerable<CustomerDTO> customers)
        {
            customerBindingSource.DataSource = customers;
        }

        public string GetTitle()
        {
            return "Клиенты";
        }

        public object GetCurrentObject()
        {
            return customerBindingSource.Current;
        }

        public void RefreshData()
        {
            var source = dataGridCustomers.DataSource;
            dataGridCustomers.DataSource = null;
            dataGridCustomers.DataSource = source;
            dataGridCustomers.Invalidate();
        }

        #endregion

        #region Event Handlers

        private void dataGridCustomers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OnEditCustomer();
        }

        private void btnEditCustomer_Click(object sender, EventArgs e)
        {
            OnEditCustomer();
        }

        private void OnEditCustomer()
        {
            if (EditCustomer == null) return;
            var customer = customerBindingSource.Current as CustomerDTO;
            if (customer != null)
                EditCustomer(customer);
            dataGridCustomers.Invalidate();
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            if (AddCustomer != null)
            {
                AddCustomer();
            }
        }

        private void btnDeleteCustomer_Click(object sender, EventArgs e)
        {
            if (DeleteCustomer == null) return;
            var customer = customerBindingSource.Current as CustomerDTO;
            if (customer != null)
                DeleteCustomer(customer);
        }

        #endregion

    }
}
