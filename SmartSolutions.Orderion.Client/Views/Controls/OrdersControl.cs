﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using SmartSolutions.Orderion.Client.ViewInterfaces.Order;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.Views.Controls
{
    public partial class OrdersControl : UserControl, IOrdersView
    {
        public OrdersControl()
        {
            InitializeComponent();
            dataGridOrders.AutoGenerateColumns = false;
            freightDataGridViewTextBoxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        public string GetTitle()
        {
            return "Заявки";
        }

        public object GetCurrentObject()
        {
            return orderDTOBindingSource.Current;
        }

        public void FillData(List<OrderDTO> orders)
        {
            orderDTOBindingSource.DataSource = orders;
        }

        public void RefreshData()
        {
            var source = dataGridOrders.DataSource;
            dataGridOrders.DataSource = null;
            dataGridOrders.DataSource = source;
            dataGridOrders.Invalidate();
        }


        public event Action<OrderDTO> EditOrder;
        public event Action<OrderDTO> DeleteOrder;

        private void dataGridOrders_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OnEditOrder();
        }

        private void btnEditOrder_Click(object sender, EventArgs e)
        {
            OnEditOrder();
        }

        private void OnEditOrder()
        {
            if (EditOrder == null) return;
            var order = orderDTOBindingSource.Current as OrderDTO;
            if (order != null)
                EditOrder(order);
            dataGridOrders.Invalidate();
        }

        private void btnDeleteOrder_Click(object sender, EventArgs e)
        {
            if (DeleteOrder == null) return;
            var order = orderDTOBindingSource.Current as OrderDTO;
            if (order != null)
                DeleteOrder(order);
        }

        private void dataGridOrders_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridOrders.Columns[e.ColumnIndex].Name == "freightDataGridViewTextBoxColumn" && e.Value != null)
            {
                e.Value = ((decimal) e.Value).ToString("F2");
                e.FormattingApplied = true;
            }


        }

    }
}
