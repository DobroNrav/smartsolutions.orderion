﻿using System.Windows.Forms;

namespace SmartSolutions.Orderion.Client.PresenterInterfaces
{
    public interface IMainPresenter
    {
        Form GetViewForm();
    }
}