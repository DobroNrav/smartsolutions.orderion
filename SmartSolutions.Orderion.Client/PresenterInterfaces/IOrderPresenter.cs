﻿using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.PresenterInterfaces
{
    public interface IOrderPresenter
    {
        void Init(OrderDTO order);
        void ShowView(); 
    }
}