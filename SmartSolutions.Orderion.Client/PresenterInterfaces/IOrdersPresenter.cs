﻿using System;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.PresenterInterfaces
{
    public interface IOrdersPresenter
    {
        event Action<OrderDTO> EditOrder;
        event Action<OrderDTO> DeleteOrder;

        IPageVeiw GetPageView();
    }
}