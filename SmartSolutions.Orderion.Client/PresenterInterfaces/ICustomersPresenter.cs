﻿using System;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.PresenterInterfaces
{
    public interface ICustomersPresenter
    {
        event Action AddCustomer;
        event Action<CustomerDTO> EditCustomer;
        event Action<CustomerDTO> DeleteCustumer;
        event Action<CustomerDTO> AddOrder;

        IPageVeiw GetPageVeiw();
    }
}
