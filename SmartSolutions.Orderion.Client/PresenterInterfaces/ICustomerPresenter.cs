﻿using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.PresenterInterfaces
{
    public interface ICustomerPresenter
    {
        void Init(CustomerDTO customer);
        void ShowView();
    }
}