﻿using System;
using System.Windows.Forms;
using log4net.Config;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.PresenterInterfaces;
using SmartSolutions.Orderion.Core.Container;

namespace SmartSolutions.Orderion.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            XmlConfigurator.Configure();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            

            ClientUnityConfigurator.ConfigureContainer(Container.MainContainer);

            var mainPresenter = Container.MainContainer.Resolve<IMainPresenter>();

            Application.Run(mainPresenter.GetViewForm());
        }
    }
}
