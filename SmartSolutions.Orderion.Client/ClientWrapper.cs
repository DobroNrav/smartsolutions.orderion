﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SmartSolutions.Orderion.Client
{
    public class ClientWrapper<T> : IDisposable where T : ICommunicationObject
    {
        private readonly T _serviceClient;

        public T ServiceClient
        {
            get { return _serviceClient; }
        }

        public ClientWrapper(T serviceClient)
        {
            if (serviceClient == null) throw new ArgumentNullException("serviceClient");
            _serviceClient = serviceClient;
        }

        public void Dispose()
        {
            if (_serviceClient.State == CommunicationState.Faulted)
            {
                _serviceClient.Abort();
            }
            else if (_serviceClient.State != CommunicationState.Closed)
            {
                _serviceClient.Close();
            }
        }
    }

}
