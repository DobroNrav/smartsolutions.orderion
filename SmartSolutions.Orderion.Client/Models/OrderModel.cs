﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Client.OrderService;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.Models
{
    public class OrderModel : IOrderModel
    {
        private List<OrderDTO> _orders;

        public event Action SourceUpdated;

        public List<OrderDTO> GetAll()
        {
            try
            {
                using (var client = new ClientWrapper<OrderServiceClient>(new OrderServiceClient()))
                {
                    if (_orders != null)
                    {
                        _orders.Clear();
                        _orders.AddRange(client.ServiceClient.GetOrders().ToList());
                    }
                    else
                        _orders = client.ServiceClient.GetOrders().ToList();

                    if (SourceUpdated != null)
                        SourceUpdated();
                    return _orders;
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.HandleException(e);
            }
            return null;
        }

        public void Save(OrderDTO order)
        {
            OrderDTO saved;
            var isTransient = order.OrderID == 0;
            try
            {
                using (var client = new ClientWrapper<OrderServiceClient>(new OrderServiceClient()))
                {
                    if (isTransient)
                    {
                        saved = client.ServiceClient.AddOrder(order, order.CustomerID);
                    }
                    else
                    {
                        saved = client.ServiceClient.UpdateOrder(order, order.CustomerID);
                    }
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.HandleException(e);
                return;
            }
            if (saved != null)
            {
                if (_orders != null)
                {
                    if (isTransient)
                    {
                        _orders.Add(saved);
                        if (SourceUpdated != null)
                        SourceUpdated();
                    }
                    else
                    {
                        var o = _orders.Find(dto => dto.OrderID == saved.OrderID);
                        if (o != null)
                            o.CopyFrom(saved);
                    }
                    
                }
            }
        }

        public void Delete(int orderId)
        {
            if (orderId == 0) return;
            try
            {
                using (var client = new ClientWrapper<OrderServiceClient>(new OrderServiceClient()))
                {
                    if (client.ServiceClient.DeleteOrder(orderId.ToString("")) && _orders != null)
                        _orders.RemoveAll(x => x.OrderID == orderId);
                    if (SourceUpdated != null)
                        SourceUpdated();
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.HandleException(e);
            }
        }

        public void RemoveLoadedOrdersByCustomerId(string customerID)
        {
            if (_orders != null)
            {
                _orders.RemoveAll(o => o.CustomerID == customerID);
                if (SourceUpdated != null)
                    SourceUpdated();
            }
        }

        
    }
}
