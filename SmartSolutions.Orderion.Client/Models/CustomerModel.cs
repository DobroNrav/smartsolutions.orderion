﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.CustomerService;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Core.Container;

namespace SmartSolutions.Orderion.Client.Models
{
    public class CustomerModel : ICustomerModel
    {
        private List<CustomerDTO> _customers = new List<CustomerDTO>();

        public event Action SourceUpdated;

        public IEnumerable<CustomerDTO> GetAll()
        {
            try
            {
                using (var client = new ClientWrapper<CustomerServiceClient>(new CustomerServiceClient()))
                {
                    if (_customers == null)
                        _customers = client.ServiceClient.GetCustomers().ToList();
                    else
                    {
                        _customers.Clear();
                        _customers.AddRange(client.ServiceClient.GetCustomers());
                    }
                    if (SourceUpdated != null)
                        SourceUpdated();
                }
                return _customers;
            }
            catch (Exception e)
            {
                ExceptionHandler.HandleException(e);
            }
            return null;
        }

        public void Save(CustomerDTO customer)
        {
            CustomerDTO saved;
            var isTransient = customer.CustomerID == null;
            try
            {
                using (var client = new ClientWrapper<CustomerServiceClient>(new CustomerServiceClient()))
                {
                    if (isTransient)
                        saved = client.ServiceClient.AddCustomer(customer);
                    else
                        saved = client.ServiceClient.UpdateCustomer(customer);
                }
            }
            catch(Exception e)
            {
                ExceptionHandler.HandleException(e);
                return;
            }

            if (saved != null)
            {
                if (_customers != null)
                {
                    if (isTransient)
                    {
                        _customers.Add(saved);
                        if (SourceUpdated != null)
                            SourceUpdated();
                    }
                    else
                    {
                        var c = _customers.Find(dto => dto.CustomerID == saved.CustomerID);
                        c.CopyFrom(saved);
                    }
                  }
            }
        }

        public void Delete(string customerID)
        {
            if (customerID == null) return;
            try
            {
                using (var client = new ClientWrapper<CustomerServiceClient>(new CustomerServiceClient()))
                {
                    if (client.ServiceClient.DeleteCustomer(customerID) && _customers != null)
                        _customers.RemoveAll(x => x.CustomerID == customerID);
                    var orderModel = Container.MainContainer.Resolve<IOrderModel>();
                    orderModel.RemoveLoadedOrdersByCustomerId(customerID);
                    if (SourceUpdated != null)
                        SourceUpdated();
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.HandleException(e);
            }
        }
    }
}
