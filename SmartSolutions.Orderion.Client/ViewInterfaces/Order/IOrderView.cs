﻿using System;
using System.Windows.Forms;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ViewInterfaces.Order
{
    public interface IOrderView
    {
        event Action<OrderDTO> Save;

        void Init(OrderDTO order);
        DialogResult ShowDialog(); 
    }
}