﻿using System;
using System.Collections.Generic;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ViewInterfaces.Order
{
    public interface IOrdersView : IPageVeiw
    {
        event Action<OrderDTO> EditOrder;
        event Action<OrderDTO> DeleteOrder;

        void FillData(List<OrderDTO> orders);
    }
}