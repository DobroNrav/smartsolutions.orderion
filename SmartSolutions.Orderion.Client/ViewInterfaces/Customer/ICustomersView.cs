﻿using System;
using System.Collections.Generic;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ViewInterfaces.Customer
{
    public interface ICustomersView : IPageVeiw
    {
        void FillData(IEnumerable<CustomerDTO> customers);

        event Action<CustomerDTO> EditCustomer;
        event Action AddCustomer;
        event Action<CustomerDTO> DeleteCustomer;
        event Action<CustomerDTO> AddOrder;

    }
}