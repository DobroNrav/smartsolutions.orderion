﻿using System;
using System.Windows.Forms;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ViewInterfaces.Customer
{
    public interface ICustomerView
    {
        event Action<CustomerDTO> Save;

        void Init(CustomerDTO customer);
        DialogResult ShowDialog();
    }
}