﻿using System;
using System.Windows.Forms;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ViewInterfaces
{
    public interface IMainView
    {
        event Action ShowAllCustomers;
        event Action AddCustomer;
        event Action EditCustomer;
        event Action DeleteCustomer;

        event Action ShowAllOrders;
        event Action AddOrder;
        event Action EditOrder;
        event Action DeleteOrder;

        void AddPage(IPageVeiw pageView);

        object GetCurrentObject();
    }
}
