﻿using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ViewInterfaces
{
    public interface IPageVeiw
    {
        void RefreshData();

        string GetTitle();
        object GetCurrentObject();
    }
}