﻿using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Client.Models;
using SmartSolutions.Orderion.Client.PresenterInterfaces;
using SmartSolutions.Orderion.Client.Presenters;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces.Customer;
using SmartSolutions.Orderion.Client.ViewInterfaces.Order;
using SmartSolutions.Orderion.Client.Views;
using SmartSolutions.Orderion.Client.Views.Controls;

namespace SmartSolutions.Orderion.Client
{
    public class ClientUnityConfigurator
    {
        public static void ConfigureContainer(IUnityContainer container)
        {
            // Views
            container
                .RegisterType<IMainView, MainView>()
                .RegisterType<ICustomersView, CustomersControl>()
                .RegisterType<IOrdersView, OrdersControl>()
                .RegisterType<ICustomerView, CustomerView>()
                .RegisterType<IOrderView, OrderView>()

                // Models
                .RegisterType<IMainModel, MainModel>()
                .RegisterType<ICustomerModel, CustomerModel>(new ContainerControlledLifetimeManager())
                .RegisterType<IOrderModel, OrderModel>(new ContainerControlledLifetimeManager())

                // Presenters
                .RegisterType<IMainPresenter, MainPresenter>()
                // Customer
                .RegisterType<ICustomersPresenter, CustomersPresenter>()
                .RegisterType<ICustomerPresenter, CustomerPresenter>()
                // Order
                .RegisterType<IOrdersPresenter, OrdersPresenter>()
                .RegisterType<IOrderPresenter, OrderPresenter>();

        }
    }
}
