﻿using System;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Client.PresenterInterfaces;
using SmartSolutions.Orderion.Client.Properties;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Core.Container;

namespace SmartSolutions.Orderion.Client.Presenters
{
    public class MainPresenter : IMainPresenter
    {
        public MainPresenter()
        {
            View = Container.MainContainer.Resolve<IMainView>();
            Model = Container.MainContainer.Resolve<IMainModel>();
            SubscribeEvents();
        }

        public IMainView View { get; set; }
        public IMainModel Model { get; set; }

        public Form GetViewForm()
        {
            return View as Form;
        }

        public void SubscribeEvents()
        {
            View.ShowAllCustomers += OnShowAllCustomers;
            View.AddCustomer += OnAddCustomer;
            View.EditCustomer += () =>
            {
                var customer = View.GetCurrentObject() as CustomerDTO;
                OnEditCustomer(customer);
            };
            View.DeleteCustomer += () =>
            {
                var customer = View.GetCurrentObject() as CustomerDTO;
                OnDeleteCustomer(customer);
            };
            View.ShowAllOrders += OnShowAllOrders;
            View.AddOrder += OnAddOrder;
            View.EditOrder += () =>
            {
                var order = View.GetCurrentObject() as OrderDTO;
                if (order == null) return;
                OnEditOrder(order);
            };
            View.DeleteOrder += () =>
            {
                var order = View.GetCurrentObject() as OrderDTO;
                OnDeleteOrder(order);
            };
        }

        private void OnShowAllOrders()
        {
            var presenter = Container.MainContainer.Resolve<IOrdersPresenter>();
            presenter.EditOrder += OnEditOrder;
            presenter.DeleteOrder += OnDeleteOrder;

            View.AddPage(presenter.GetPageView());
        }

        private void OnDeleteOrder(OrderDTO order)
        {
            if (order == null || order.CustomerID == null) return;
            if (MessageBox.Show("Вы действительно хотите удалить заявку?", Resources.Warning, MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var model = Container.MainContainer.Resolve<IOrderModel>();
                model.Delete(order.OrderID);
            }
        }

        private void OnAddOrder()
        {
            var presenter = Container.MainContainer.Resolve<IOrderPresenter>();
            var customer = View.GetCurrentObject() as CustomerDTO;
            if (customer == null)
            {
                MessageBox.Show("Невозможно добавить заявку. Выберите заказчика в справочнике клиентов и повторите попытку", Resources.Warning, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var order = new OrderDTO {CustomerID = customer.CustomerID};
            presenter.Init(order);
            presenter.ShowView();
        }

        private void OnEditOrder(OrderDTO order)
        {
            var presenter = Container.MainContainer.Resolve<IOrderPresenter>();
            presenter.Init(order);
            presenter.ShowView();
        }

        private void OnShowAllCustomers()
        {
            var presenter = Container.MainContainer.Resolve<ICustomersPresenter>();
            presenter.EditCustomer += OnEditCustomer;
            presenter.AddCustomer += OnAddCustomer;
            presenter.DeleteCustumer += OnDeleteCustomer;
            
            View.AddPage(presenter.GetPageVeiw());
        }

        private void OnAddCustomer()
        {
            var presenter = Container.MainContainer.Resolve<ICustomerPresenter>();
            presenter.Init(new CustomerDTO());
            presenter.ShowView();
        }

        private void OnEditCustomer(CustomerDTO customer)
        {
            if (customer == null) return;
            var presenter = Container.MainContainer.Resolve<ICustomerPresenter>();
            presenter.Init(customer);
            presenter.ShowView();
        }

        private void OnDeleteCustomer(CustomerDTO customer)
        {
            if (customer == null || customer.CustomerID == null) return;
            if (MessageBox.Show("Вы действительно хотите удалить клиента со всеми его заказами?", Resources.Warning, MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var model = Container.MainContainer.Resolve<ICustomerModel>();
                model.Delete(customer.CustomerID);
            }
        }
        
        
    }
}
