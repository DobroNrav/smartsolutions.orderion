﻿using System;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Client.PresenterInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces.Order;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Core.Container;

namespace SmartSolutions.Orderion.Client.Presenters
{
    public class OrdersPresenter : IOrdersPresenter
    {

        public OrdersPresenter()
        {
            View = Container.MainContainer.Resolve<IOrdersView>();
            Model = Container.MainContainer.Resolve<IOrderModel>();
            View.FillData(Model.GetAll());
            Model.SourceUpdated += View.RefreshData;
        }

        public IOrdersView View { get; set; }
        public IOrderModel Model { get; set; }

        public IPageVeiw GetPageView()
        {
            return View;
        }

        public event Action<OrderDTO> EditOrder
        {
            add { View.EditOrder += value; }
            remove { View.EditOrder -= value; }
        }
        public event Action<OrderDTO> DeleteOrder
        {
            add { View.DeleteOrder += value; }
            remove { View.DeleteOrder -= value; }
        }

    }
}
