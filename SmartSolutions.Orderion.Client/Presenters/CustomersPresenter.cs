﻿using System;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Client.PresenterInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces.Customer;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Core.Container;

namespace SmartSolutions.Orderion.Client.Presenters
{
    public class CustomersPresenter : ICustomersPresenter
    {
        public ICustomersView View { get; set; }
        public ICustomerModel Model { get; set; }

        public CustomersPresenter()
        {
            View = Container.MainContainer.Resolve<ICustomersView>();
            Model = Container.MainContainer.Resolve<ICustomerModel>();
            View.FillData(Model.GetAll());
            Model.SourceUpdated += View.RefreshData;
        }

        public event Action AddCustomer
        {
            add { View.AddCustomer += value; }
            remove { View.AddCustomer -= value; }
        }
        public event Action<CustomerDTO> EditCustomer
        {
            add { View.EditCustomer += value; }
            remove { View.EditCustomer -= value; }
        }

        public event Action<CustomerDTO> DeleteCustumer
        {
            add { View.DeleteCustomer += value; }
            remove { View.DeleteCustomer -= value; }
        }

        public event Action<CustomerDTO> AddOrder
        {
            add { View.AddOrder += value; }
            remove { View.AddOrder -= value; }
        }

        public IPageVeiw GetPageVeiw()
        {
            return View;
        }
    }
}
