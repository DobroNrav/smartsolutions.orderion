﻿using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Client.PresenterInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces.Customer;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Core.Container;

namespace SmartSolutions.Orderion.Client.Presenters
{
    public class CustomerPresenter : ICustomerPresenter
    {
        public CustomerPresenter()
        {
            View = Container.MainContainer.Resolve<ICustomerView>();
            Model = Container.MainContainer.Resolve<ICustomerModel>();
            View.Save += OnSave;
        }

        public ICustomerModel Model { get; set; }
        public ICustomerView View { get; set; }

        public void Init(CustomerDTO customer)
        {
            View.Init(customer);
        }

        public void ShowView()
        {
            View.ShowDialog();
        }

        private void OnSave(CustomerDTO customer)
        {
            Model.Save(customer);
        }
    }
}
