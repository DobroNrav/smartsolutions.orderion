﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Client.ModelInterfaces;
using SmartSolutions.Orderion.Client.PresenterInterfaces;
using SmartSolutions.Orderion.Client.ViewInterfaces.Order;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Core.Container;

namespace SmartSolutions.Orderion.Client.Presenters
{
    public class OrderPresenter : IOrderPresenter
    {
        public OrderPresenter()
        {
            View = Container.MainContainer.Resolve<IOrderView>();
            Model = Container.MainContainer.Resolve<IOrderModel>();
        }

        public IOrderView View { get; set; }

        public IOrderModel Model { get; set; }

        public void Init(OrderDTO order)
        {
            View.Init(order);
            View.Save += OnSave;
        }

        private void OnSave(OrderDTO obj)
        {
            Model.Save(obj);
        }

        public void ShowView()
        {
            View.ShowDialog();
        }
    }
}
