﻿using System;
using System.Collections.Generic;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ModelInterfaces
{
    public interface ICustomerModel
    {
        event Action SourceUpdated;

        IEnumerable<CustomerDTO> GetAll();
        void Save(CustomerDTO customer);
        void Delete(string customerID);
    }
}