﻿using System;
using System.Collections.Generic;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Client.ModelInterfaces
{
    public interface IOrderModel
    {
        List<OrderDTO> GetAll();
        void Save(OrderDTO order);
        void Delete(int orderId);
        void RemoveLoadedOrdersByCustomerId(string customerID);

        event Action SourceUpdated;
    }
}