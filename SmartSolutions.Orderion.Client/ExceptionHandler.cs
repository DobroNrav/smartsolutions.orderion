﻿using System;
using System.Reflection;
using System.Windows.Forms;
using log4net;
using SmartSolutions.Orderion.Client.Properties;

namespace SmartSolutions.Orderion.Client
{
    public class ExceptionHandler
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static Exception HandleException(Exception e)
        {
            MessageBox.Show(Resources.ApplicationErrorText + e.Message, Resources.ApplicationErrorTitle, MessageBoxButtons.OK);
            Log.Error(e.Message, e);
            return e;
        }
    }
}
