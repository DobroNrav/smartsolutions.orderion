﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SmartSolutions.Orderion.WindowsWcfServer
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {
            var servicesToRun = new ServiceBase[] 
            { 
                new OrderionHost() 
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
