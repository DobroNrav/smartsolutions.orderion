﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace SmartSolutions.Orderion.WindowsWcfServer
{
    [RunInstaller(true)]
    public partial class OrderionServerInstaller : System.Configuration.Install.Installer
    {
        public OrderionServerInstaller()
        {
            InitializeComponent();

            var process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.NetworkService;
            
            var service = new ServiceInstaller();
            service.ServiceName = "OrderionServer";
            service.DisplayName = "Orderion Server";
            service.Description = "Hosts Orderion Services";
            service.StartType = ServiceStartMode.Automatic;

            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
