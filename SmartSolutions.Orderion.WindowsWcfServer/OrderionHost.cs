﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SmartSolutions.Orderion.Services;

namespace SmartSolutions.Orderion.WindowsWcfServer
{
    public partial class OrderionHost : ServiceBase
    {
        private ServiceHost _serviceHost = null;

        public OrderionHost()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                var httpBaseAddress = new Uri("http://localhost:4321/OrderionService");
                _serviceHost = new ServiceHost(typeof (CustomerService), new []{httpBaseAddress});

                _serviceHost.AddServiceEndpoint(typeof(ICustomerService), new BasicHttpBinding(), "");
                
                var smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                _serviceHost.Description.Behaviors.Add(smb);

                _serviceHost.Open();
            }
            catch (Exception e)
            {
                _serviceHost = null;
            }
        }

        protected override void OnStop()
        {
            if (_serviceHost != null)
            {
                _serviceHost.Close();
                _serviceHost = null;
            }
        }
    }
}
