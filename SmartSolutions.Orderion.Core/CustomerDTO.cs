﻿using System;
using System.Runtime.Serialization;

namespace SmartSolutions.Orderion.Core
{
    [Serializable]
    [DataContract]
    public class CustomerDTO
    {
        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string ContactTitle { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Region { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Fax { get; set; }

        public override string ToString()
        {
            return string.Format("{0} ({1}, {2})", CompanyName, City, Country);
        }
    }
}
