﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartSolutions.Orderion.Core.Mappers;

namespace SmartSolutions.Orderion.Core
{
    public static class Extenstions
    {
        public static void CopyFrom(this CustomerDTO destination, CustomerDTO source)
        {
            SelfMapper.Copy(source, destination);
        }

        public static void CopyFrom(this OrderDTO destination, OrderDTO source)
        {
            SelfMapper.Copy(source, destination);
        }
    }
}
