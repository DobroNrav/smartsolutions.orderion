﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace SmartSolutions.Orderion.Core.Container
{
    public class Container
    {
        internal class Nested
        {
            internal static readonly IUnityContainer MainContainer = new UnityContainer();    
        }

        public static IUnityContainer MainContainer{get { return Nested.MainContainer; }}

        public static void ConfigureContainer(UnityConfigurationSection unityConfigSection)
        {
            Nested.MainContainer.LoadConfiguration(unityConfigSection);
        }
    }
}
