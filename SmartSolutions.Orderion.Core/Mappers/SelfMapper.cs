﻿using AutoMapper;

namespace SmartSolutions.Orderion.Core.Mappers
{
    public class SelfMapper
    {
        static SelfMapper()
        {
            Mapper.CreateMap<CustomerDTO, CustomerDTO>();
            Mapper.CreateMap<OrderDTO, OrderDTO>();
        }

        public static void Copy<T>(T source, T destination)
        {
            Mapper.Map(source, destination);
        }
    }
}
