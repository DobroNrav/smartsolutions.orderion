﻿using System;
using System.ServiceModel;
using log4net.Config;
using SmartSolutions.Orderion.Services;

namespace SmartSolutions.Orderion.WcfServerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            ServiceUnityConfigurator.Configure(Core.Container.Container.MainContainer);

            try
            {
                var host = new ServiceHost(typeof (CustomerService));
                host.Open();
                host = new ServiceHost(typeof(OrderService));
                host.Open();

                Console.WriteLine("Сервер запущен");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при запуске сервера: " + ex.Message);
                Console.ReadLine();
            }

        }
    }
}
