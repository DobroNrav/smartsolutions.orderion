﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace SmartSolutions.Orderion.WcfServerConsole
{
    public class ServiceHostManager
    {
        const string _baseAddress = "http://localhost:4321/OrderionService";

        private readonly List<ServiceHost> _hosts = new List<ServiceHost>();

        public ServiceHostManager Register(Type serviceContract, Type serviceType, string path)
        {
            var httpUri = new Uri(_baseAddress + "/" + path);
            var host = new ServiceHost(serviceType, httpUri);

            var smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            host.Description.Behaviors.Add(smb);

            var debug = host.Description.Behaviors.Find<ServiceDebugBehavior>();
            if (debug == null)
            {
                host.Description.Behaviors.Add(new ServiceDebugBehavior { IncludeExceptionDetailInFaults = true });
            }
            else
            {
                if (!debug.IncludeExceptionDetailInFaults)
                {
                    debug.IncludeExceptionDetailInFaults = true;
                }
            }

            host.AddServiceEndpoint(serviceContract, new BasicHttpBinding(), "");

            host.Faulted += HostOnFaulted;
            _hosts.Add(host);

            return this;
        }

        private void HostOnFaulted(object sender, EventArgs eventArgs)
        {
            Console.WriteLine("Host is Faulted");
            Console.WriteLine(sender);
            Console.WriteLine(eventArgs);
        }

        public void Open()
        {
            foreach (var serviceHost in _hosts)
            {
                serviceHost.Open();
            }
        }

        public void Close()
        {
            foreach (var serviceHost in _hosts)
            {
                serviceHost.Close();
            }
        }
    }
}
