﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Data;
using SmartSolutions.Orderion.Data.Mappers;

namespace SmartSolutions.Orderion.Services
{
    public class OrderService : BaseService, IOrderService
    {

        public List<OrderDTO> GetOrders()
        {
            try
            {
                using (var repo = Repository)
                {
                    return repo.Orders.Select(x => DTOMapper.Map<Order, OrderDTO>(x)).ToList();
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        public List<OrderDTO> GetOrdersByCustomerId(string id)
        {
            try
            {
                using (var repo = Repository)
                {
                    return
                        repo.Orders.Where(o => o.CustomerID == id)
                            .Select(o => DTOMapper.Map<Order, OrderDTO>(o))
                            .ToList();
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        public OrderDTO AddOrder(OrderDTO orderDto, string customerId)
        {
            try
            {
                using (var repo = Repository)
                {
                    return repo.CreateOrder(orderDto, customerId);
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        public OrderDTO UpdateOrder(OrderDTO orderDto, string customerId)
        {
            try
            {
                using (var repo = Repository)
                {
                    return repo.UpdateOrder(orderDto);
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        public bool DeleteOrder(string id)
        {
            try
            {
                using (var repo = Repository)
                {
                    int i;
                    if (int.TryParse(id, out i))
                        return repo.DeleteOrder(i);
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
            return false;
        }
    }
}