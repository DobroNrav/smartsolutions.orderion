﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Services
{
    [ServiceContract]
    public interface IOrderService
    {

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Orders/")]
        List<OrderDTO> GetOrders();

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/{id}/Orders/")]
        List<OrderDTO> GetOrdersByCustomerId(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/{customerId}/Orders/")]
        OrderDTO AddOrder(OrderDTO orderDto, string customerId);

        [OperationContract]
        [WebInvoke(Method = "PUT", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/{customerId}/Orders/")]
        OrderDTO UpdateOrder(OrderDTO orderDto, string customerId);

        [OperationContract]
        [WebInvoke(Method = "DELETE", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Orders/{id}")]
        bool DeleteOrder(string id);

    }
}
