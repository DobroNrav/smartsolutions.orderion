﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using SmartSolutions.Orderion.Core;

namespace SmartSolutions.Orderion.Services
{
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/")]
        List<CustomerDTO> GetCustomers();

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/{id}")]
        CustomerDTO GetCustomer(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/")]
        CustomerDTO AddCustomer(CustomerDTO customerDto);
    
        [OperationContract]
        [WebInvoke(Method = "PUT", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/")]
        CustomerDTO UpdateCustomer(CustomerDTO customerDto);

        [OperationContract]
        [WebInvoke(Method = "DELETE", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, UriTemplate = "Customers/{id}")]
        bool DeleteCustomer(string id);

    }
}
