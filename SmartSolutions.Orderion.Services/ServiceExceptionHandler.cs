﻿using System;
using System.Reflection;
using log4net;

namespace SmartSolutions.Orderion.Services
{
    public class ServiceExceptionHandler
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static Exception HandleException(Exception e)
        {
            Log.Error(e.Message, e);
            return e;
        }
    }
}