﻿using System.Reflection;
using log4net;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Data;

namespace SmartSolutions.Orderion.Services
{
    public class BaseService
    {
        private static IRepository _repository;

        public IRepository Repository
        {
            get
            {
                if (_repository == null)
                    _repository = Core.Container.Container.MainContainer.Resolve<IRepository>();
                return _repository;
            }
        }

    }
}