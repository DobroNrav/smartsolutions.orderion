﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartSolutions.Orderion.Core;
using SmartSolutions.Orderion.Data;
using SmartSolutions.Orderion.Data.Mappers;

namespace SmartSolutions.Orderion.Services
{
    public class CustomerService : BaseService, ICustomerService
    {
        #region ICustomerService Members

        public List<CustomerDTO> GetCustomers()
        {
            try
            {
                using (var repo = Repository)
                {
                    return repo.Customers.Select(x => DTOMapper.Map<Customer, CustomerDTO>(x)).ToList();
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
            return null;
        }

        public CustomerDTO GetCustomer(string id)
        {
            try
            {
                using (var repo = Repository)
                {
                    return DTOMapper.Map<Customer, CustomerDTO>(repo.Customers.FirstOrDefault(c => c.CustomerID == id));
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        public CustomerDTO AddCustomer(CustomerDTO customerDto)
        {
            try
            {
                using (var repo = Repository)
                {
                    return repo.CreateCustomer(customerDto);
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        public CustomerDTO UpdateCustomer(CustomerDTO customerDto)
        {
            try
            {
                using (var repo = Repository)
                {
                    return repo.UpdateCustomer(customerDto);
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        public bool DeleteCustomer(string id)
        {
            try
            {
                using (var repo = Repository)
                {
                    return repo.DeleteCustomer(id);
                }
            }
            catch (Exception e)
            {
                ServiceExceptionHandler.HandleException(e);
                throw;
            }
        }

        #endregion
    }
}
