﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using SmartSolutions.Orderion.Data;
using SmartSolutions.Orderion.Data.Mappers;

namespace SmartSolutions.Orderion.Services
{
    public class ServiceUnityConfigurator
    {
        public static void Configure(IUnityContainer container)
        {
            container
                .RegisterType<IRepository, SqlRepository>(new ContainerControlledLifetimeManager());
        }
    }
}